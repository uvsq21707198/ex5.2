/**
 * Created by m.najafi on 10/19/2017.
 */


public class Fraction implements Comparable<Fraction> {

    public static final Fraction ZERO = new Fraction();
    public static final Fraction UN = new Fraction(1);


    public final int numérateur;
    public final int dénominateur;
    public  final double value;

    public Fraction(int numérateur, int dénominateur) {
        this.numérateur = numérateur;
        this.dénominateur = dénominateur;
        value = numérateur/(double)dénominateur;
    }

    public Fraction() {
        this(0, 1);
    }

    public Fraction(int numérateur) {
        this(numérateur, 1);
    }

    public Fraction add(Fraction fraction) {
        if (fraction.dénominateur == this.dénominateur) {
            return new Fraction(numérateur + fraction.numérateur, fraction.dénominateur);
        } else {
            return new Fraction(((this.numérateur * fraction.dénominateur) + (this.dénominateur * fraction.numérateur)), fraction.dénominateur * this.dénominateur);

        }
    }


    @Override
    public boolean equals(Object obj) {
        return compareTo((Fraction) obj) == 0;
    }


    @Override
    public String toString() {
        String str;
        str = numérateur + "/" + dénominateur;
        return str;
    }

    private static int pgcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return pgcd(b, a % b);
    }


    @Override
    public int compareTo(Fraction fraction) {
        //return 0 if a == b
        //return 1 if a > b
        //return -1 if a < b

        int pgcd = pgcd(this.dénominateur, fraction.dénominateur);
        int ppmc = (fraction.dénominateur * this.dénominateur) / pgcd;
        if (this.numérateur * (ppmc / this.dénominateur) > (fraction.numérateur * (ppmc / fraction.dénominateur))) {
            return 1;
        } else if (this.numérateur * (ppmc / this.dénominateur) == (fraction.numérateur * (ppmc / fraction.dénominateur))) {
            return 0;
        } else {
            return -1;
        }
    }
}



